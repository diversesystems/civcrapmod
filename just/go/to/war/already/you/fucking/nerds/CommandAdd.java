package just.go.to.war.already.you.fucking.nerds;

import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.IClientCommand;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandAdd extends CommandBase implements IClientCommand {
	@Override
	public boolean allowUsageWithoutPrefix(ICommandSender sender, String message) {
		return false;
	}

	@Override
	public String getName() {
		return "renadd";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/renadd";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length > 0) {
			if (args.length == 2) {
				if (args[0].trim().equals("friend") && !args[1].trim().isEmpty()) {
					if (!Renaynay.friends.contains(args[1]) && !Renaynay.enemies.contains(args[1]))
						Renaynay.friends.add(args[1]);
				} else if (args[0].trim().equals("enemy") && !args[1].trim().isEmpty()) {
					if (!Renaynay.friends.contains(args[1]) && !Renaynay.enemies.contains(args[1]))
						Renaynay.enemies.add(args[1]);
				} else
					Minecraft.getMinecraft().player.sendMessage(new TextComponentString("You fucked up"));
			}
		}
	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return true;
	}

	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1)
			return getListOfStringsMatchingLastWord(args, Lists.newArrayList("enemy", "friend"));
		else if (args.length == 2) {
			ArrayList<String> temp = new ArrayList<>();

			if (Minecraft.getMinecraft().player.connection.getPlayerInfoMap() != null) {
				for (NetworkPlayerInfo np : Minecraft.getMinecraft().player.connection.getPlayerInfoMap()) {
					if (!np.getGameProfile().getName().equals(Minecraft.getMinecraft().player.getName()))
						temp.add(np.getGameProfile().getName());
				}
			}

			return getListOfStringsMatchingLastWord(args, temp);
		}

		return Collections.emptyList();
	}

}
