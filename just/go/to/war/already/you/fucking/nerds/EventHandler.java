package just.go.to.war.already.you.fucking.nerds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

import java.util.Random;

public class EventHandler {

	public static boolean enable = true;

	private long time;
	private double posX, posY, posZ;

	public EventHandler(Renaynay renaynay) {

	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void onRenderWorld(final RenderWorldLastEvent event) throws Exception {

		if (event != null && Minecraft.getMinecraft().world != null && Minecraft.getMinecraft().currentScreen == null && enable) {
			for (Entity found : Minecraft.getMinecraft().world.loadedEntityList) {

				if (isValid(found)) {

					posX = found.lastTickPosX + (found.posX - found.lastTickPosX) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosX;
					posY = found.lastTickPosY + (found.posY - found.lastTickPosY) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosY;
					posZ = found.lastTickPosZ + (found.posZ - found.lastTickPosZ) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosZ;

					//Hi angels
					if (found.getName().equals("centipede777") || found.getName().equals("Greenkitten") || found.getName().equals("HanTzu") || found.getName().equals("daddo69") )
						someHulaHoopShit(posX, posY + 1.0f, posZ, .45f, .75f, 0);

					else if (Renaynay.friends.contains(found.getName()))
						someHulaHoopShit(posX, posY + 1.0f, posZ, .45f, .75f, 1);
					else if (Renaynay.enemies.contains(found.getName()))
						someHulaHoopShit(posX, posY + 1.0f, posZ, .45f, .75f, -1);
					else
						someHulaHoopShit(posX, posY + 1.0f, posZ, .45f, .75f, 69);
				}
			}

		}
	}

	private void someHulaHoopShit(double x, double y, double z, float radius, float opacity, int relationship) {
		GL11.glPushMatrix();
		GlStateManager.disableTexture2D();
		GlStateManager.disableDepth();
		GlStateManager.enableBlend();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glDepthMask(false);

		double tau = Math.PI * 2;
		double fans = 45.0;
		Random rand = new Random();

		GL11.glLineWidth(2.5f);

		GL11.glBegin(1);

		if (relationship == 1)
			GL11.glColor4f(.0f, 1f, .4f, opacity);
		else if (relationship == -1)
			GL11.glColor4f(1f, .2f, .2f, opacity);
		else if (relationship == 69)
			GL11.glColor4f(1f, 1f, .2f, opacity);

		for (int i = 0; i < 90; i++) {
			if (relationship == 0)
				GL11.glColor4f((float) rand.nextFloat(), (float) rand.nextFloat(), (float) rand.nextFloat(), 1.0f);

			GL11.glVertex3d(x + radius * Math.cos(i * tau / fans), y, z + radius * Math.sin(i * tau / fans));
		}

		GL11.glEnd();

		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glDepthMask(true);
		GlStateManager.enableDepth();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
		GL11.glLineWidth(1.5f);
		GL11.glPopMatrix();

	}

	public static boolean isValid(final Entity player) {
		return player != null && player != Minecraft.getMinecraft().player && !player.isDead && player.isEntityAlive() && player instanceof EntityPlayer && player instanceof EntityOtherPlayerMP;
	}

}
