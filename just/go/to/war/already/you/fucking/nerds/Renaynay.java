package just.go.to.war.already.you.fucking.nerds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import org.lwjgl.input.Keyboard;

import java.io.*;
import java.util.HashSet;

@Mod(modid = "renaynay", name = "stop talking about war and actually go to war you nerds or go outside for once, it's fucking elegos; want support or a mod? commission me :/", version = "send me thighs as payment :/ made you look")

public class Renaynay {

	private File configFolder;
	private static File friendsFile;
	private static File enemiesFile;

	public static HashSet<String> friends, enemies;

	private KeyBinding enable = new KeyBinding("Turns it on and off :/ made you look", Keyboard.KEY_LMENU, "Renaynay :/ made you look");


	@Mod.EventHandler
	public void onPreInit(FMLPreInitializationEvent e) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(Minecraft.getMinecraft().mcDataDir).append("//config//Renaynay");
		configFolder = new File(sb.toString());

		if (!configFolder.exists())
			configFolder.mkdirs();

		sb = new StringBuilder();
		sb.append(configFolder).append("//RenaynayFriends.txt");
		friendsFile = new File(sb.toString());

		sb = new StringBuilder();
		sb.append(configFolder).append("//RenaynayEnemies.txt");
		enemiesFile = new File(sb.toString());

		if (!friendsFile.exists())
			friendsFile.createNewFile();

		if (!enemiesFile.exists())
			enemiesFile.createNewFile();

		friends = new HashSet<>();
		enemies = new HashSet<>();

		loadFriends(friendsFile);
		loadEnemies(enemiesFile);

	}

	@Mod.EventHandler
	public void onInit(FMLInitializationEvent e) {
		MinecraftForge.EVENT_BUS.register(this);
		FMLCommonHandler.instance().bus().register(this);

		ClientRegistry.registerKeyBinding(enable);

		ClientCommandHandler.instance.registerCommand(new CommandAdd());
		ClientCommandHandler.instance.registerCommand(new CommandRemove());
		MinecraftForge.EVENT_BUS.register(new EventHandler(this));
	}

	@SubscribeEvent
	public void onKeyInput(final InputEvent.KeyInputEvent event) throws Exception {
		if (this.enable.isPressed()) {
			EventHandler.enable = !EventHandler.enable;
		}

	}

	@SubscribeEvent
	public void onMouse(MouseEvent e) throws Exception {
		if (!e.isButtonstate())
			return;

		RayTraceResult pos;
		String name;

		if (e.getButton() == 2) {
			pos = Minecraft.getMinecraft().objectMouseOver;

			if (pos == null || pos.entityHit == null || !(pos.entityHit instanceof EntityPlayer) || !pos.typeOfHit.equals(RayTraceResult.Type.ENTITY))
				return;

			name = pos.entityHit.getName();

			if (!friends.contains(name) && !enemies.contains(name)) {
				friends.add(name);
				Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Added " + name + " to friends"));
			} else if (friends.contains(name)) {
				friends.remove(name);
				Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Removed " + name + " from friends"));
			}

			saveFriends();
		}

	}


	private void loadEnemies(File enemiesFile) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(enemiesFile));
		String read = br.readLine();

		while (read != null) {
			if (!read.trim().isEmpty()) {
				if (!enemies.contains(read) && !friends.contains(read)) {
					enemies.add(read);
				}
			}
			read = br.readLine();
		}

		br.close();
	}

	private void loadFriends(File friendsFile) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(friendsFile));
		String read = br.readLine();

		while (read != null) {
			if (!read.trim().isEmpty()) {
				if (!enemies.contains(read) && !friends.contains(read)) {
					friends.add(read);
				}
			}
			read = br.readLine();
		}

		br.close();
	}

	public static void saveFriends() throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(friendsFile));

		String[] temp = friends.toArray(new String[0]);
		StringBuilder sb;

		for (String s : temp) {
			if (s != null && !s.isEmpty()) {
				sb = new StringBuilder();
				sb.append(s).append("\n");
				bw.write(sb.toString());
			}
		}
		bw.close();
	}

	public static void saveEnemies() throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(enemiesFile));

		String[] temp = enemies.toArray(new String[0]);
		StringBuilder sb;

		for (String s : temp) {
			if (s != null && !s.isEmpty()) {
				sb = new StringBuilder();
				sb.append(s).append("\n");
				bw.write(sb.toString());
			}
		}
		bw.close();
	}


}
